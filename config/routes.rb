Rails.application.routes.draw do
  get 'sessions/new'

  get 'users/new'

  root                's_tatic_pages#home'
  get    'help'    => 's_tatic_pages#help'
  get    'about'   => 's_tatic_pages#about'
  get    'contact' => 's_tatic_pages#contact'
  get    'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :users
end